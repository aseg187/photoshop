// The objective of this file is to install Adobe Camera Raw v12
#include "Photoshop.h"
#include "Cameraraw.h"

#include <stdio.h>
// Use malloc and exit
#include <stdlib.h>
// Use this for make syscalls
#include <sys/sysinfo.h>
// Use this for atoi, conversor string to int, strcmp...
#include <string.h>

int install_cameraraw()
{
    system("notify-send 'Photoshop CC' 'Starting Installer of Camera Raw' --icon=$PWD/images/photoshop-cc.png");
    char *location = malloc(sizeof(char) * 150);
    if (location == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    printf("What is the installation path that you have written in the installer? (Absolute Path):\n");
    scanf("%s", location);
    printf("Installing Adobe Camera Raw v12...");
    // Download Camera Raw .exe
    char *command = malloc(sizeof(char) * 350);
    if (command == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    sprintf(command, "cd %s/PhotoshopCC; wget https://download.adobe.com/pub/adobe/photoshop/cameraraw/win/12.x/CameraRaw_12_2_1.exe", location);
    system(command);
    free(command);
    // Install
    command = malloc(sizeof(char) * 350);
    if (command == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    sprintf(command, "export WINEPREFIX=%s/PhotoshopCC; cd %s/PhotoshopCC; wine CameraRaw_12_2_1.exe", location, location);
    system(command);
    // Free memory of location and command
    free(location);
    free(command);
    printf("Adobe Camera Raw v12 installed successfully!");
    return_program();
};