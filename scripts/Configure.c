// The objective of this file it is to configure a prefix created
#include "Photoshop.h"
#include "Configure.h"
#include <stdio.h>
// Use malloc and exit
#include <stdlib.h>
// Use this for make syscalls
#include <sys/sysinfo.h>
// Use this for atoi, conversor string to int, strcmp...
#include <string.h>

int configure_wine()
{
    char *location = malloc(sizeof(char) * 200);
    if (location == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    printf("What is the installation path that you have written in the installer? (Absolute Path):\n");
    scanf("%s", location);
    char *wine_command = malloc(sizeof(char) * 250);
    if (wine_command == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    sprintf(wine_command, "WINEPREFIX=%s/.prefix winecfg", location);
    printf("%s", wine_command);
    system(wine_command);
    // Free memory of wine command and location
    free(wine_command);
    free(location);
    return_program();
}