/* 
The objective of this file is install all the necessary stable 
dependencies and configure it based on linux distribution.
*/
#include "Photoshop.h"
#include "Installer.h"
#include <stdio.h>
// Use malloc and exit
#include <stdlib.h>
// Use this for make syscalls
#include <sys/sysinfo.h>
// Use this for atoi, conversor string to int, strcmp...
#include <string.h>

void setup_fedora();
void setup_debian();
void set_dark_mode(char *location);
void change_desktop(char *location);

int install_photoshop()
{
    // Inform to user that the installation is started
    system("notify-send 'Photoshop CC' 'Starting Installer of Photoshop' --icon=$PWD/images/photoshop-cc.png");
    // Depending on distribution, make the installation with their
    // corresponding managment package
    char *distribution = malloc(sizeof(char) * 4);
    printf("Do you have an Ubuntu or a Ubuntu based distro (if is other Debian based distro or Debian now write no)? [yes/no]: ");
    scanf("%s", distribution);
    // Install for Ubuntu or based distros except Debian or based (the installation is different)
    if (distribution[0] == 'y')
    {
        // Installing Wine from standard Ubuntu Repository
        system("sudo apt-get -y install wine64 wget");
    }
    else
    {
        printf("Do you have an Debian or any based distro except Ubuntu? [yes/no]: ");
        scanf("%s", distribution);
        if (distribution[0]  == 'y')
        {
            setup_debian();
        }
        else
        {
            printf("Do you have an Arch Linux or any based distro? [yes/no]: ");
            scanf("%s", distribution);
            if (distribution[0]  == 'y')
            {
                printf("Installing WineHQ Stable...");
                system("sudo pacman -S wine wget");
            }
            else
            {
                printf("Do you have a Fedora distro?: [yes/no]: ");
                scanf("%s", distribution);
                if (distribution[0]  == 'y')
                {
                    setup_fedora();
                }
                else
                {
                    printf("The distribution you are using is not supported\n");
                    free(distribution);
                    return 1;
                }
            }
        }

    }
    // Free memory of the answer of distribution
    free(distribution);
    /* 
    Once specific installation is done, now configure all
    necessary tools for run Photoshop and download it.
    */
    // First install winetricks
    printf("Installing winetricks...");
    system("wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks; chmod +x winetricks; sudo mv winetricks /usr/local/bin/");
    // Ask for the installation location
    printf("Where do you want to install Photoshop (Absolute Route)?:\n");
    printf("Example of route (realize that in the end it doesn't end with /): /home/your_username\n");
    char *location = malloc(sizeof(char) * 150);
    if (location == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    scanf("%s", location);
    // Create directory structure, set wine prefix, install winetricks and configure it with winecfg.
    printf("Click on Install to everything it asks for.");
    char *prefix = malloc(sizeof(char) * 350);
    if (prefix == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    // Is important the argument -f for force, beacuse if checksum sha256 fail, the program can't continue
    sprintf(prefix, 
            "mkdir %s/PhotoshopCC; WINEPREFIX=%s/PhotoshopCC winetricks -f vcrun2019 vcrun2012 vcrun2013 vcrun2010 fontsmooth=rgb gdiplus msxml3 msxml6 atmlib corefonts dxvk", 
            location, location);
    system(prefix);
    free(prefix);
    // Open winecfg for config the prefix
    prefix = malloc(sizeof(char) * 250);
    sprintf(prefix, "WINEPREFIX=%s/PhotoshopCC winecfg", location);
    system(prefix);
    free(prefix);
    // Set dark mode of wine
    set_dark_mode(location);
    // Download Photoshop and move inside .prefix
    char *installation_location = malloc(sizeof(char) * 350);
    if (installation_location == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    sprintf(installation_location, "tar -xf ./files/PhotoshopCC.tar.xz -C %s/PhotoshopCC", location);
    system(installation_location);
    free(installation_location);
    // Copy icon and .desktop for appear in gnome apps
    system("sudo cp ./images/photoshop-cc.png /usr/share/icons/hicolor/256x256/apps");
    // Finally ask if want install Photoshop in all system or only in his current user
    char *answer = malloc(sizeof(char) * 5);
    if (answer == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    printf("Do you want to install Photoshop in your user or in all[only/all] ?: ");
    scanf("%s", answer);
    change_desktop(location);
    if (answer[0] == 'o')
    {
        system("mv ./files/photoshop.desktop $HOME/.local/share/applications");
    }
    else
    {
        system("sudo mv ./files/photoshop.desktop /usr/share/applications");
    };
    // Free memory of location
    free(location);
    free(answer);
    // Inform the user that everything is complete
    system("notify-send 'Photoshop CC' 'Finishing the installation...' --icon=/usr/share/icons/hicolor/256x256/apps/photoshop.desktop");
    printf("Installation completed successfully! Opening Photoshop CC v 21.0.2");
    system("gtk-launch photoshop");
    return_program();
}

void setup_fedora()
{
    // First update repos and packages
    system("sudo dnf upgrade");
    // Next install required packages, for this I want use the latest stable
    // version from the official repo
    system("sudo dnf -y install dnf-plugins-core");
    // Adding official repo for download WineHQ depending on the version of Fedora
    printf("What version of Fedora do you have?: ");
    int *version = malloc(sizeof(int));
    if (version == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
    }
    scanf("%i", version);
    char *command = malloc(sizeof(char) * 100);
    if (command == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
    }
    sprintf(command, "sudo dnf config-manager --add-repo https://dl.winehq.org/wine-builds/fedora/%i/winehq.repo", *version);
    system(command);
    // Free memory of version and command
    free(version);
    free(command);
    // Installing WineHQ stable and winetricks
    printf("Installing WineHQ Stable...");
    // Using the argument --allowerasing because by default Fedora has a package of Wine installed
    system("sudo dnf -y install --allowerasing winehq-stable wget");
    /*
    One very important thing, add the Wine Stable installation to PATH, because by default it is not added.
    If you have a different shell than the default one on your system, the following command will only work 
    until you close the terminal. Then it will not detect the installation of Wine,
    although photoshop will open as a normal app.
    */
    system("export PATH=\"/opt/wine-stable/bin:${PATH}\"");
}

void setup_debian()
{
    system("sudo apt-get update && apt-get upgrade");
    // Enabling 32 bit architecture
    system("sudo dpkg --add-architecture i386");
    // Add WineHQ repository
    system("sudo apt -y install gnupg2 software-properties-common wget");
    system("wget -qO - https://dl.winehq.org/wine-builds/winehq.key | sudo apt-key add -");
    system("sudo apt-add-repository https://dl.winehq.org/wine-builds/debian/");
    // Update APT
    system("sudo apt update");
    system("wget -O- -q https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/Debian_10/Release.key | sudo apt-key add -");
    system("echo 'deb http://download.opensuse.org/repositories/Emulators:/Wine:/Debian/Debian_10 ./' | sudo tee /etc/apt/sources.list.d/wine-obs.list");
    // Finally install WineHQ from stable branch
    system("sudo apt update");
    printf("Installing WineHQ Stable...");
    system("sudo apt install --install-recommends winehq-stable");
    /*
    One very important thing, add the Wine Stable installation to PATH, because by default it is not added.
    If you have a different shell than the default one on your system, the following command will only work 
    until you close the terminal. Then it will not detect the installation of Wine,
    although photoshop will open as a normal app.
    */
    system("export PATH=\"/opt/wine-stable/bin:${PATH}\"");
}

void set_dark_mode(char *location)
{
    // Create string type for manage array of dark color, use \ for escape characters
    char colors[31][38] = 
    {
        {"[Control Panel\\Colors] 1491939580"},
        {"#time=1d2b2fb5c69191c"},
        {"\"ActiveBorder\"=\"49 54 58\""},
        {"\"ActiveTitle\"=\"49 54 58\""},
        {"\"AppWorkSpace\"=\"60 64 72\""},
        {"\"Background\"=\"49 54 58\""},
        {"\"ButtonAlternativeFace\"=\"200 0 0\""},
        {"\"ButtonDkShadow\"=\"154 154 154\""},
        {"\"ButtonFace\"=\"49 54 58\""},
        {"\"ButtonHilight\"=\"119 126 140\""},
        {"\"ButtonLight\"=\"60 64 72\""},
        {"\"ButtonShadow\"=\"60 64 72\""},
        {"\"ButtonText\"=\"219 220 222\""},
        {"\"GradientActiveTitle\"=\"49 54 58\""},
        {"\"GradientInactiveTitle\"=\"49 54 58\""},
        {"\"GrayText\"=\"155 155 155\""},
        {"\"Hilight\"=\"119 126 140\""},
        {"\"HilightText\"=\"255 255 255\""},
        {"\"InactiveBorder\"=\"49 54 58\""},
        {"\"InactiveTitle\"=\"49 54 58\""},
        {"\"InactiveTitleText\"=\"219 220 222\""},
        {"\"InfoText\"=\"159 167 180\""},
        {"\"InfoWindow\"=\"49 54 58\""},
        {"\"Menu\"=\"49 54 58\""},
        {"\"MenuBar\"=\"49 54 58\""},
        {"\"MenuHilight\"=\"119 126 140\""},
        {"\"MenuText\"=\"219 220 222\""},
        {"\"Scrollbar\"=\"73 78 88\""},
        {"\"TitleText\"=\"219 220 222\""},
        {"\"Window\"=\"35 38 41\""},
        {"\"WindowFrame\"=\"49 54 58\""},
        {"\"WindowText\"=\"219 220 222\""}
    }; 
    // Adding configuration to file
    printf("Setting dark...");
    // Accesing to file user.reg
    char *location_reg = malloc(sizeof(char) * 200);
    if (location_reg == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    sprintf(location_reg, "%s/PhotoshopCC/user.reg", location);
    // Write in the final of file (append mode)
    FILE *user_reg = fopen(location_reg, "a");
    if (user_reg == NULL)
    {
        fprintf(stderr, "Can't open user.reg file.");
        exit(1);
    }
    for (int i = 0; i < 31; i++) 
    {
        for (int j = 0; j < 38; j++) 
        {
            if (colors[i][j] != '\0')
            {
                fprintf(user_reg, "%c", colors[i][j]);
            }
        }
        fprintf(user_reg, "\n");
    }
    // Close file
    fclose(user_reg);
    // Free memory of location reg
    free(location_reg);
}

void change_desktop(char *location)
{
    // Open the file .desktop and change the Exec command for start Photoshop
    // Exec command for open Photoshop.exe file
    char *location_exe = malloc(sizeof(char) * 300);
    if (location_exe == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    sprintf(location_exe, "Exec=env WINEPREFIX=\"%s/PhotoshopCC\" wine64 %s/PhotoshopCC/Photoshop-CC/Photoshop.exe", location, 
            location);
    // Copy .desktop file (if the user want another installation in other location this is useful)
    system("cp ./files/photoshop-cc.desktop ./files/photoshop.desktop");
    // Open file
    FILE *location_desktop = fopen("./files/photoshop.desktop", "a");
    if (location_desktop == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    // Change the text Exec=CHANGE to the command location_exe
    fprintf(location_desktop, "%s", location_exe);
    // Close the file of desktop
    fclose(location_desktop);
    // Free memory of location_exe
    free(location_exe);
}
