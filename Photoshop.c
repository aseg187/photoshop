#include <stdio.h>
// Use exit
#include <stdlib.h>
// Including all headers files
#include "./scripts/Requirements.h"
#include "./scripts/Installer.h"
#include "./scripts/Cameraraw.h"
#include "./scripts/Configure.h"
#include "./scripts/Uninstaller.h"

/*
Thanks especially to Gictorbit, because his script has served me as a reference and 
I have added some things: https://github.com/Gictorbit/photoshopCClinux
Also thanks to the people at WineHQ, especially the maintainers Bartosz Kosiorek 
and Baalaji Balasubramani for publishing 
their tests in v21: https://appdb.winehq.org/objectManager.php?sClass=version&iId=38516
As they have done with v22 of Photoshop: https://appdb.winehq.org/objectManager.php?sClass=version&iId=39717
(which this version of Photosop, has been published in October 2020 and is being 
updated today 07-26-2021!!).
Unfortunately v22 has not worked for me, although it may be because my PC does not 
meet any requirement (since they have been increased).

The version of this Photoshop is 21.0.2
*/

int main()
{
    int option = 0;
    while (1)
    {
        system("clear");
        printf("Welcome to Photoshop CC v21 Linux Installer\n");
        printf("Choose one of the following options:\n");
        printf("1. Check if your computer meets the requirements to run Photoshop\n");
        printf("2. Install Photoshop\n");
        printf("3. Install Adobe Camera Raw v12\n");
        printf("4. Configure Wine\n");
        printf("5. Uninstall Photoshop\n");
        printf("6. Exit the program\n");
        scanf("%i", &option);
        if (option == 1)
        {
            check_requirements();
        }
        else if (option == 2)
        {
            install_photoshop();
        }
        else if (option == 3)
        {
            install_cameraraw();
        }
        else if (option == 4)
        {
            configure_wine();
        }
        else if (option == 5)
        {
            uninstall_photoshop();
        }
        else if (option == 6)
        {
            return 0;
        }
    }
}

void return_program()
{
    getchar();
    // Return
    char answer;
    printf("\nDo you want to go back to the main menu?[n,y] ");
    scanf("%c", &answer);
    if (answer == 'y')
    {
        return 0;
    }
    else
    {
        exit(0);
    }
}
